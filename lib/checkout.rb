Product = Struct.new(:code, :price)

class Checkout
  attr_reader :events
  attr_reader :products

  def initialize(events)
    @events = events
    @products = []
  end

  def scan(product)
    products.push(product)
  end

  def price
    price_with_events(events, products)
  end

  private

  def price_with_events(events, products)
    done = events.reduce(total: 0, products: products) do |discount_sum, event|
      discounted = event.price(discount_sum[:products])
      { 
        total: discount_sum[:total] + discounted[:total],
        products: discounted[:products] 
      }
    end

    {
      total: done[:total] + done[:products].reduce(0) { |sum, p| sum + p.price },
      products: done[:products] 
    }

  end
end
