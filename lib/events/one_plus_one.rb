class OnePlusOne
  attr_reader :event_item
  attr_reader :products
  attr_reader :oneplusone_total

  def initialize(event_item)
    @event_item = event_item
  end

  def price(products)
    @products = products
    if is_event?
    { total: oneplusone_total,
      products: non_event_products.concat(odd_event_item).flatten
    }
    else
      { total: 0, products: products }
    end

  end

  private

  def is_event?
    @oneplusone_total = event_products.flatten.reduce(0) { |sum, p| sum + p.price } / 2
  end

  def event_products 
    couple_event_products.select { |p| p.length > 1 }
  end

  def couple_event_products
    products.select { |p| p.code == event_item }.each_slice(2).to_a
  end

  def odd_event_item
    couple_event_products.select { |p| p.length == 1 }
  end

  def non_event_products
    products.select { |p| p.code != event_item }
  end

end
