class VolumeDiscount
  attr_reader :event_item
  attr_reader :threshold
  attr_reader :volume_price
  attr_reader :products

  def initialize(event_item, threshold, volume_price)
    @event_item = event_item
    @threshold = threshold
    @volume_price = volume_price
  end

  def price(products)
    @products = products
    if is_event?
      {
        total: volume_total,
        products: non_event_products.concat(remained_item).flatten 
      }
    else
      { 
        total: 0, 
        products: products 
      }
    end
  end

  private

  def is_event?
    event_products.length >= threshold 
  end

  def volume_event
    slice_event_products.select { |p| p.length >= threshold }
  end

  def remained_item
    slice_event_products.select { |p| p.length < threshold }
  end

  def volume_total 
    volume_event.length * volume_price
  end 

  def slice_event_products
    products.select { |p| p.code == event_item }.each_slice(threshold).to_a
  end

  def event_products
    products.select { |p| p.code == event_item }
  end

  def non_event_products
    products.select { |p| p.code != event_item }
  end

end
