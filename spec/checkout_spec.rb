require_relative "../lib/checkout.rb"

describe Checkout do
  let(:a) { Product.new("A", 50) }
  let(:b) { Product.new("B",30) }
  let(:c) { Product.new("C", 20) }
  let(:d) { Product.new("D", 15) }

  describe "1, total without any event" do
    let(:checkout) { Checkout.new([]) }
    it "1-1,return total properly" do
      [a, a].each { |p| checkout.scan p }
      expect(checkout.price[:total]).to eql(100)
    end 
    it "1-2,return total properly" do
      [a, a, b, b, c , d].each { |p| checkout.scan p }
      expect(checkout.price[:total]).to eql(195)
    end 
  end

  describe "2, including events" do
    let(:oneplusone) { OnePlusOne.new("A") }
    let(:volumediscount) { VolumeDiscount.new("A", 3, 130) }
    let(:checkout) { Checkout.new([ oneplusone, volumediscount ]) }

    it "2-1,total sum for products of prices with one plus one event" do
      [a, a].each { |p| checkout.scan p }
      expect(checkout.price[:total]).to eql(50)
    end

    it "2-2,total sum for products of prices with one plus one event" do
      [a, a, b, c, d].each { |p| checkout.scan p }
      expect(checkout.price[:total]).to eql(115)
    end

    it "2-3,total sum for products of prices with volume discount event" do
      [a, a, b, a, c, d].each { |p| checkout.scan p }
      expect(checkout.price[:total]).to eql(165)
      expect(checkout.price[:products]).to match_array [b, a, c, d]
    end
  end

  describe "3, including muliple events" do
    let(:oneplusone) { OnePlusOne.new("A") }
    let(:oneplusone1) { OnePlusOne.new("B") }
    let(:volumediscount) { VolumeDiscount.new("A", 3, 130) }
    let(:volumediscount1) { VolumeDiscount.new("B", 3, 70) }
    let(:checkout) { Checkout.new([ volumediscount, volumediscount1 ,oneplusone, oneplusone1 ]) }

    it "3-1,total sum for products of prices with one plus one event" do
      [a, a, b, b].each { |p| checkout.scan p }
      expect(checkout.price[:total]).to eql(80)
      expect(checkout.price[:products]).to match_array []
    end

    it "3-2,total sum for products of prices with one plus one event" do
      [a, a, a, b, b, c, d].each { |p| checkout.scan p }
      expect(checkout.price[:total]).to eql(195)
      expect(checkout.price[:products]).to match_array [c, d]
    end

    it "3-3,total sum for products of prices with volume discount event" do
      [a, a, a, a, b, b, b , a, c, d].each { |p| checkout.scan p }
      expect(checkout.price[:total]).to eql(285)
      expect(checkout.price[:products]).to match_array [c, d]
    end
  end

  describe "4, including muliple events" do
    let(:oneplusone) { OnePlusOne.new("A") }
    let(:oneplusone1) { OnePlusOne.new("B") }
    let(:volumediscount) { VolumeDiscount.new("A", 3, 130) }
    let(:volumediscount1) { VolumeDiscount.new("B", 3, 70) }
    let(:checkout) { Checkout.new([ oneplusone, oneplusone1 ,volumediscount, volumediscount1 ]) }

    it "4-1,total sum for products of prices with one plus one event" do
      [a, a, b, b].each { |p| checkout.scan p }
      expect(checkout.price[:total]).to eql(80)
      expect(checkout.price[:products]).to match_array []
    end

    it "4-2,total sum for products of prices with one plus one event" do
      [a, a, a, b, b, c, d].each { |p| checkout.scan p }
      expect(checkout.price[:total]).to eql(165)
      expect(checkout.price[:products]).to match_array [a, c, d]
    end

    it "4-3,total sum for products of prices with volume discount event" do
      [a, a, a, a, b, b, b , a, c, d].each { |p| checkout.scan p }
      expect(checkout.price[:total]).to eql(245)
      expect(checkout.price[:products]).to match_array [b, a, c, d]
    end
  end
  
end
