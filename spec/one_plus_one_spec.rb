require_relative "../lib/events/one_plus_one.rb"

describe OnePlusOne do
  let(:a) { Product.new("A", 50) }
  let(:b) { Product.new("B", 30) }
  let(:c) { Product.new("C", 20) }
  let(:d) { Product.new("D", 15) }

  describe "1,no events products" do
    it "returns zero" do
      onepone = OnePlusOne.new("A")
      done = onepone.price([a, b, c, d, b, c, d])
      expect(done[:total]).to eql(0)
      expect(done[:products]).to match_array [a, b, c, d, b, c, d]
    end
  end

  describe "2,including events products" do
    it "returns total discount price" do
      onepone = OnePlusOne.new("A")
      done = onepone.price([a, a, b, c, d])
      expect(done[:total]).to eql(50)
      expect(done[:products]).to match_array [b, c, d]
    end
  end

  describe "3,including events products with even number" do
    it "returns total discount price" do
      onepone = OnePlusOne.new("A")
      done = onepone.price([a, a, a, a, a, b, c, d])
      expect(done[:total]).to eql(100)
      expect(done[:products]).to match_array [a, b, c, d]
    end
  end

end
