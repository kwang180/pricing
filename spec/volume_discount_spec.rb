require_relative "../lib/events/volume_discount.rb"

describe VolumeDiscount do
  let(:a) { Product.new("A", 50) }
  let(:b) { Product.new("B", 30) }
  let(:c) { Product.new("C", 20) }
  let(:d) { Product.new("D", 15) }

  describe "1:test under threshold for VolumeDiscount" do
    it "returns zero" do
      volume = VolumeDiscount.new("A", 3, 130)
      done = volume.price([a, a, b, c, d])
      expect(done[:total]).to eql(0) 
      expect(done[:products]).to match_array [a, a, b, c, d]
    end
  end

  describe "2,test reach at theshond for VolumeDiscount" do
    it "returns the summed proferly" do
      volume = VolumeDiscount.new("A", 3, 130)
      done = volume.price([a, a, a, a, b, c, d])
      expect(done[:total]).to eql(130)
      expect(done[:products]).to match_array [a, b, c, d]
    end
  end

  describe "3,test reach over theshond for VolumeDiscount" do
    it "returns the summed proferly" do
      volume = VolumeDiscount.new("A", 3, 130)
      done = volume.price([a, a, a, a, a, a, b, c, d])
      expect(done[:total]).to eql(260)
      expect(done[:products]).to match_array [b, c, d]
    end
  end

end
